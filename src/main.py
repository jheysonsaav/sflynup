import os
import platform
from commands.install import install


def main():
    os.system("clear")
    print("-------------------- Sflynup ---------------\n")
    if platform.system() == "Windows":
        install.Windows()
    elif platform.system() == "Linux":
        install.Linux()
    elif platform.system() == "Darwin":
        install.Darwin()
    else:
        print("we could not detect your operating system\n")


if __name__ == "__main__":
    main()
