import requests
import os
import zipfile
import shutil


class install:
    def Linux():
        packageName = "sflynlang-x86_64-unknown-linux-gnu.zip"
        path = os.path.expanduser("~/.sflyn")
        packageurl = f"https://github.com/danielsolartech/ga-rust/releases/latest/download/{packageName}"
        package = requests.get(packageurl)
        if os.path.isdir(path):
            shutil.rmtree(path)
        os.mkdir(path)
        open(f"{path}/{packageName}", "wb").write(package.content)
        zipfile.ZipFile(f"{path}/{packageName}").extractall(path=f"{path}")
        os.remove(f"{path}/{packageName}")
        print(
            "sflyn has been installed on your system successfully!!\n\nHappy hacking in Linux!!\n"
        )

    def Windows():
        packageName = "sflynlang-x86_64-pc-windows-msvc.zip"
        path = os.path.expanduser("~/.sflyn" or "%USERPROFILE%/.sflyn")
        packageurl = f"https://github.com/danielsolartech/ga-rust/releases/latest/download/{packageName}"
        package = requests.get(packageurl)
        if path:
            shutil.rmtree(path)
        os.mkdir(path)
        open(f"{path}/{packageName}", "wb").write(package.content)
        zipfile.ZipFile(f"{path}/{packageName}").extractall(path=f"{path}")
        os.remove(f"{path}/{packageName}")
        print(
            "sflyn has been installed on your system successfully!!\nHappy hacking in Windows!!\n"
        )

    def Darwin():
        packageName = "sflynlang-x86_64-apple-darwin.zip"
        path = os.path.expanduser("~/.sflyn")
        packageurl = f"https://github.com/danielsolartech/ga-rust/releases/latest/download/{packageName}"
        package = requests.get(packageurl)
        if path:
            shutil.rmtree(path)
        os.mkdir(path)
        open(f"{path}/{packageName}", "wb").write(package.content)
        zipfile.ZipFile(f"{path}/{packageName}").extractall(path=f"{path}")
        os.remove(f"{path}/{packageName}")
        print(
            "sflyn has been installed on your system successfully!!\n\nHappy hacking in MacOs!!\n"
        )
